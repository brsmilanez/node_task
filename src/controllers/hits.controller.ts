import Hits from '../models/hits';
import { Request, Response } from 'express';

const getHits = async (req: Request, res: Response) => {
	try {
		/*
        Expect req.query to be:
        {
            page: 1
            author: 'author'
            title: 'title'
            _tags: 'tag1,tag2,...tagn'
        }
         */
		const { page = 1, author, _tags, title } = req.query;
		let optionPagination = {
			page,
			limit: 5,
		};
		let filters_: { [key: string]: any } = {
			deleted: false,
		};
		if (author) {
			filters_.author = author.toString();
		}
		if (title) {
			filters_.title = title.toString();
		}
		if (_tags) {
			filters_._tags = { $all: _tags.toString().split(',') };
		}
		const hits = await Hits.paginate(filters_, optionPagination);
		return res.json(hits);
	} catch (error) {
		console.log({ error });
		return res.status(500).json({ error: 'Error on get hits' });
	}
};

const deleteHit = async (req: Request, res: Response) => {
	try {
		const { id } = req.params;
		if (!id) {
			return res.status(400).json({ error: 'Missing id' });
		}
		const hit = await Hits.findOneAndUpdate(
			{ objectID: id },
			{ deleted: true }
		);
		if (!hit) {
			return res.status(404).json({ error: 'Hit not found' });
		}
		return res.json({
			status: 'Hit removed',
		});
	} catch (error) {
		console.log({ error });
		return res.status(500).json({ error: 'Error on delete hit' });
	}
};
export { getHits, deleteHit };
