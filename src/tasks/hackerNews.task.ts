import axios from 'axios';
import Hits from '../models/hits';

const urlAPIHackerNews =
	'https://hn.algolia.com/api/v1/search_by_date?query=nodejs';

const GetDataHackerNews = async () => {
	console.log({ task: 'Start task GetDataHackerNews' });
	try {
		let newData = true;
		let data = (await axios.get(urlAPIHackerNews)).data;
		const pages = data.nbPages;
		for (let i = 1; i <= pages; i++) {
			//find if hit exist in my db else create it
			for (let hit of data.hits) {
				try {
					const newHit = await Hits.create(hit);
					newHit && newHit.save();
				} catch (error) {
					if (error.code === 11000) {
						newData = false;
						break;
					}
					//if error is other than 11000 (duplicate key error)
					console.log({ error });
					newData = false;
					break;
				}
			}
			if (!newData) {
				break;
			}
			//delay for avoid to overload the api
			await new Promise((resolve) => setTimeout(resolve, 100));
			//get data from next page
			data = (await axios.get(`${urlAPIHackerNews}&page=${i}`)).data;
		}
	} catch (error) {
		console.log({ error });
	}
	console.log({
		task: 'End task GetDataHackerNews',
	});
	return;
};

export default GetDataHackerNews;
