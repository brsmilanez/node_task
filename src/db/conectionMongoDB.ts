import mongoose, { ConnectOptions } from 'mongoose';

const dbConnection = async () => {
	try {
		const urlMongo = process.env.MONGODB_CONNECTION_URL || '';
		if (!urlMongo) {
			throw new Error(
				'No se ha definido la url de conexión a la base de datos MongoDB'
			);
		}
		await mongoose.connect(urlMongo, {
			useNewUrlParser: true,
			useUnifiedTopology: true,
		} as ConnectOptions);
		console.log('Base de datos MongoDB Online');
	} catch (error) {
		console.log({ error });
		throw new Error('Error al conectar a la base de datos MongoDB');
	}
};

export default dbConnection;
