const { Schema, model } = require('mongoose');
import mongoosePaginate from 'mongoose-paginate-v2';

const HitsSchema = new Schema({
	objectID: {
		type: String,
		unique: true,
	},
	created_at: {
		type: Date,
		default: Date.now,
	},
	title: {
		type: String,
		default: null,
	},
	url: {
		type: String,
		default: null,
	},
	author: {
		type: String,
		default: null,
	},
	points: {
		type: Number,
		default: null,
	},
	story_text: {
		type: String,
		default: null,
	},
	comment_text: {
		type: String,
		default: null,
	},
	num_comments: {
		type: Number,
		default: null,
	},
	story_id: {
		type: Number,
		default: null,
	},
	story_title: {
		type: String,
		default: null,
	},
	story_url: {
		type: String,
		default: null,
	},
	parent_id: {
		type: Number,
		default: null,
	},
	created_at_i: {
		type: Number,
		default: null,
	},
	_tags: {
		type: [String],
		default: null,
	},
	_highlightResult: {
		type: Object,
		default: null,
	},
	deleted: {
		type: Boolean,
		default: false,
	},
});

HitsSchema.plugin(mongoosePaginate);

HitsSchema.methods.toJSON = function () {
	const { _id, deleted, __v, ...object } = this.toObject();
	return object;
};

export default model('Hits', HitsSchema);
