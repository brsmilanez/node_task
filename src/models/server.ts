import express from 'express';
import cors from 'cors';
import schedule from 'node-schedule';
import GetDataHackerNews from '../tasks/hackerNews.task';
import HitsRoute from '../routes/hits.route';
import dbConnection from '../db/conectionMongoDB';

class Server {
	private app: express.Application;
	private port: string | number;
	private apiRoutes: { [key: string]: string };

	constructor() {
		this.app = express();
		this.port = process.env.PORT || 8080;
		this.apiRoutes = {
			hits: '/api/hits',
		};
		this.connectDb();
		this.middlewares();
		this.routes();
		this.tasks();
	}
	async connectDb() {
		dbConnection();
	}
	tasks() {
		schedule.scheduleJob('0 * * * *', GetDataHackerNews);
	}
	middlewares() {
		this.app.use(cors());
		this.app.use(express.json());
	}
	routes() {
		this.app.use(this.apiRoutes.hits, HitsRoute);
	}
	listen() {
		this.app.listen(this.port, () => {
			console.log(`Servidor corriendo en puerto ${this.port}`);
		});
	}
}

export default Server;
