import { Router } from 'express';
import { deleteHit, getHits } from '../controllers/hits.controller';
const router = Router();

router.get('/', [], getHits);
router.delete('/:id', [], deleteHit);

export default router;
