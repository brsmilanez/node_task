- dev:
  npm run dev
- build:
  npm run build
- start:
  npm run build
  npm run start

- .env configuration:
  MONGODB_CONNECTION_URL: Add your environment variable for the MongoDB connection URL

- host deployed for testing: https://testtasknode.herokuapp.com
- example get api:
- by page 1:
  {{host}}/api/hits?page=1
- by author:
  {{host}}/api/hits?page=1&author=hsbauauvhabzb
- by tags:
  {{host}}/api/hits?page=1&_tags=comment,author_hsbauauvhabzb

- example delete element:
  (delete) {{host}}/api/hits/<objectID>
